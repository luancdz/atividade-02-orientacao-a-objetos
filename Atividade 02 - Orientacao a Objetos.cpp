// Atividade 02 - Orientacao a Objetos.cpp : Define o ponto de entrada para a aplica��o de console.
//

#include "stdafx.h"
#include <iostream>
#include "Produto.h"
#include <string>
#include "Data.h"
#include "Vetor.h"

using namespace std;
int main()
{
	Produto p1 = Produto(1, "PRODUTO 1", 4.5, 6);
	Produto p2 = Produto(2, "PRODUTO 2", 7.1, 2);
	Produto p3 = Produto(3, "PRODUTO 3", 1.3, 4);
	/*p1.imprime();
	p2.imprime();
	p3.imprime();

	cout << "declaracao p4" << endl;*/
	Produto p4(p1);
	p4.imprime();
	cout << "declaracao p5" << endl;
	Produto p5 = p1;
	cout << "declaracao p6" << endl;
	Produto p6;
	p6 = p1;
	string aux = "Joogo da vida;";

	const Produto p7 = 10;
	Produto p8 = aux;
	Data data;
	data.exibeCalendario();

	Vetor vetor = Vetor(4);

	system("PAUSE");
	return 0;
}

