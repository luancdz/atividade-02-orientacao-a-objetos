#include "stdafx.h"
#include "Produto.h"
#include <iostream>

using namespace std;
Produto::Produto()
{
	cout << "chamando Produto()" << endl;
}
/*
Produto::Produto(const Produto & p)
{
	cout << "Construto de copia executado..." << endl;
}*/

Produto::Produto(string NOME)
{
	this->nome = NOME;
}

Produto::Produto(int ID)
{
	this->id = ID;
}

Produto::Produto(int ID, string NOME, float PRECO, int QUANTIDADE)
{
	this->id = ID;
	this->nome = NOME;
	this->preco = PRECO;
	this->quantidade = QUANTIDADE;
}


Produto::~Produto()
{
	cout << "chamando ~Produto()" << endl;
}

void Produto::imprime()
{
	cout << "Id: " << id << endl;
	cout << "Nome: " << nome << endl;
	cout << "Preco: " << preco << endl;
	cout << "Quantidade: " << quantidade << endl;
	
}
