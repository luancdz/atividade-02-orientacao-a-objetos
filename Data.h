#pragma once
class Data
{
private:
	int dia;
	int mes;
	int ano;
	Data* dataVenda;
	//Data data[4][6];

public:
	Data();
	~Data();
	void setDia(int dia);
	void setMes(int mes);
	void setAno(int ano);
	void setDataVenda(Data* dataVenda);
	int getDia();
	int getMes();
	int getAno();
	Data getDataVenda();
	void exibeCalendario();
};

