#pragma once
#include <string>

using namespace std;
class Produto
{
private:
	int id;
	string nome;
	float preco;
	int quantidade;
public:
	Produto();
//	Produto(const Produto &p);
	Produto(string NOME);
	Produto(int ID);
	Produto(int ID, string NOME, float PRECO, int QUANTIDADE);
	~Produto();
	void imprime();
};

